// web.js
var http = require('http');
var static = require('node-static');
// http server
var app = http.createServer(handler);
app.listen(5000);
var file = new static.Server('./public');


// handler for http req
function handler(req, res) {
	//var pathname = url.parse(req.url).pathname;
	file.serve(req, res);
}

var port = Number(process.env.PORT || 5000);
app.listen(port, function() {
	console.log("Listening on " + port);
});